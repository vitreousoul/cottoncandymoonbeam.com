# Cotton Candy Moonbeam

<section id="about">
  <p>
    Cotton Candy Moonbeam is a celebration of the beauty and complexity of Lindenmayer systems.
  </p>
  <p>
    Exploration begins at the constant system, which is a straight line.
    Connected to this initial system is a complex expanse, which contains shapes of all sorts.
  </p>
  <p>
    Each system has parameters, theta and depth, that affect the projected shape.
    Changes to these parameters project systems into shapes that range from beautifully self-similar to unnervingly hairballish.
  </p>
  <p>
    The gallery offers curated entry points into the exploration space.
  </p>
</section>


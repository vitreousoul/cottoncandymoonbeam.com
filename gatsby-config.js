const fs = require('fs');
const path = require('path');

function resolveFilepath(name, path) {
  return {
    resolve: `gatsby-source-filesystem`,
    options: { name, path }
  };
}

module.exports = {
  siteMetadata: {
    title: `cotton candy moonbeam`,
    description: `Celebrating the beauty and complexity of L-Systems.`,
    author: `Ryan Merritt`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-netlify`,
    resolveFilepath('images', `${__dirname}/src/images`),
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        implementation: require('sass')
      }
    },
    resolveFilepath( `readme-md`, `${__dirname}` ),
    `gatsby-transformer-remark`,
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}

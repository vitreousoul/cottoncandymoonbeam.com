type stack_accumulator =
  { count: int * int
  ; last_symbol: Symbol.t option
  ; production: Symbol.production
  }

let validate_stack_accumulator count =
  let ( open_count, close_count ) = count in
  open_count == close_count

let next_count ( push_count, pop_count ) = function
  | Symbol.Push ->
      ( push_count + 1, pop_count )
  | Symbol.Pop ->
      ( push_count, pop_count + 1 )
  | _ ->
      ( push_count, pop_count )

let stack_is_valid production =
  let rec rstack_is_valid stack_accumulator =
    match stack_accumulator with
    | None ->
        false
    | Some({ count; last_symbol; production }) ->
        (match production with
        | [] ->
            validate_stack_accumulator count
        | symbol :: production ->
            if symbol == Symbol.Pop && last_symbol == Some(Symbol.Push) then
              false
            else
              let count =
                next_count count symbol in
              let accumulator =
                { count; last_symbol = Some(symbol); production = production } in
              rstack_is_valid @@ Some accumulator)
  in
  rstack_is_valid @@ Some { count = (0, 0); last_symbol = None; production }

let stack_production push pop production =
  let mapper i symbol =
    if i == push then
      Symbol.Push
    else if i == pop then
      Symbol.Pop
    else
      symbol in
  production
    |> Utils.list_rev_mapi mapper

let rec stacker production push pop stack =
  let length = List.length production in
  match pop == length with
  | true ->
      if push < length - 3 then
        stacker production (push + 1) (push + 3) stack
      else
        stack
  | false ->
      let next_production =
        stack_production push pop production in
      let next_stack =
        next_production :: stack in
      stacker production push (pop + 1) next_stack

let stacks_not_equal stack_a stack_b =
  match List.compare_lengths stack_a stack_b == 0 with
  | false ->
      false
  | true ->
      let folder not_equal a b =
        if not_equal then
          a != b
        else
          true in
      List.fold_left2 folder false stack_a stack_b

let stacks_of_production production =
  let length = List.length production in
  match length > 2 with
  | false ->
      []
  | true ->
      stacker production 0 2 []
        |> List.filter stack_is_valid
        |> List.filter @@ stacks_not_equal production

let stacks_of_rules rules =
  let mapper ( symbol, production ) =
    stacks_of_production production
      |> List.rev_map ( fun p -> Rules.add symbol p rules )
  in
  Rules.bindings rules
    |> List.rev_map mapper
    |> Utils.list_flatten


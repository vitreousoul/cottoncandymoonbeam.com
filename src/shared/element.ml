let map l f =
  (* TODO: consider using rev_map *)
  List.map f l
    |> Array.of_list
    |> React.array

let mapi f l =
  (* TODO: consider using rev_map *)
  List.mapi f l
    |> Array.of_list
    |> React.array

let empty =
  React.array [||]


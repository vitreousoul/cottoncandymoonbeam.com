let replacements =
  Symbol.[ A; B; Plus; Minus ]

let is_replaceable symbol =
  Symbol.(
    match symbol with
    | Push | Pop -> false
    | _ -> true
  )

let replacements_of_symbol symbol =
  match is_replaceable symbol with
  | false -> []
  | true ->
      replacements
        |> List.filter (fun s -> symbol != s)

let replacements_of_production index production =
  match List.nth_opt production index with
  | None ->
      []
  | Some(symbol) ->
      let replacements =
        replacements_of_symbol symbol in
      let replacement_map r =
        let production_map i s =
          if i == index then r else s in
        production
          |> Utils.list_rev_mapi production_map in
      replacements
        |> List.rev_map replacement_map
        |> List.rev

let replacements_of_rules rules =
  let bindings_mapper ( symbol, production ) =
    let mapper i _ =
      replacements_of_production i production in
    production
      |> List.mapi mapper
      |> Utils.list_flatten
      |> List.map (fun replacement -> Rules.add symbol replacement rules)
  in
  Rules.bindings rules
    |> List.map bindings_mapper
    |> Utils.list_flatten

let extensions_of_production production =
  replacements
    |> List.rev_map (fun r -> r :: production)
    |> List.rev

let extensions_of_rules rules =
  let mapper ( symbol, production ) =
    extensions_of_production production
      |> List.rev_map ( fun p -> Rules.add symbol p rules ) in
  Rules.bindings rules
    |> List.rev_map mapper
    |> List.rev
    |> Utils.list_flatten


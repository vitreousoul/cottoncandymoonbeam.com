let axiom =
  Symbol.[ A ]

let producer production rules =
  let folder next_production symbol =
    List.fold_left (fun a b -> b :: a) next_production (Rules.apply symbol rules)
  in
  production
    |> List.fold_left folder []
    |> List.rev

let rec produce production rules depth =
  match depth == 0 with
  | true ->
      production
  | false ->
      produce (producer production rules) rules (depth - 1)


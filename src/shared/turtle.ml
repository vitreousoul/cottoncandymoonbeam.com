type frame =
  { heading: Vector.Polar.t
  ; position: Vector.Cartesian.t
  }

type viewbox =
  { min: Vector.Cartesian.t
  ; max: Vector.Cartesian.t
  }

type t =
  { stack: frame Stack.t
  ; frame: frame
  ; path: string
  ; viewbox: viewbox
  }

type action =
  | Line
  | Rotate of float
  | Push
  | Pop

let action_of_symbol theta symbol =
  match symbol with
  | Symbol.A -> Line
  | Symbol.B -> Line
  | Symbol.Plus -> Rotate(theta)
  | Symbol.Minus -> Rotate(-.theta)
  | Symbol.Push -> Push
  | Symbol.Pop -> Pop

let empty_frame =
  { heading = { r = 20. ; t = 0. }
  ; position = { x = 0. ; y = 0. }
  }

let empty_cartesian =
  Vector.Cartesian.
  { x = 0.
  ; y = 0.
  }

let string_of_position Vector.Cartesian.{ x ; y } =
  Js.Float.toString x ^ " " ^ Js.Float.toString y

let append_path label path position =
  path ^ " " ^ label ^ string_of_position position

let empty =
  { stack = Stack.create ()
  ; frame = empty_frame
  ; path = append_path "M" "" empty_frame.position
  ; viewbox =
      { min = empty_cartesian
      ; max = empty_cartesian
      }
  }

let move_turtle frame =
  { frame with
    position = Vector.Cartesian.( add frame.position (of_polar frame.heading) )
  }

let move_turtle should_draw turtle =
  let label =
    match should_draw with
    | true -> "L"
    | false -> "M" in
  let frame = move_turtle turtle.frame in
  { turtle with
    frame = frame
  ; path = append_path label turtle.path frame.position
  ; viewbox =
      { min = Vector.Cartesian.min turtle.viewbox.min frame.position
      ; max = Vector.Cartesian.max turtle.viewbox.max frame.position
      }
  }

let rotate_turtle turtle angle =
  { turtle with
    frame =
      { turtle.frame with
        heading =
          Vector.Polar.{ turtle.frame.heading with t = turtle.frame.heading.t +. angle }
      }
  }

let push_turtle turtle =
  let () = Stack.push turtle.frame turtle.stack in
  turtle

let pop_turtle turtle =
  match Stack.is_empty turtle.stack with
  | true ->
      turtle
  | false ->
      let frame = Stack.pop turtle.stack in
      { turtle with
        frame = frame
      ; path = append_path "M" turtle.path frame.position
      }

let turtle_of_action turtle action =
  match action with
  | Line ->
      move_turtle true turtle
  | Rotate(angle) ->
      rotate_turtle turtle angle
  | Push ->
      push_turtle turtle
  | Pop ->
      pop_turtle turtle

let of_production ?(theta=0.523599) production =
  production
    |> List.rev_map (action_of_symbol theta)
    |> List.rev
    |> List.fold_left turtle_of_action empty


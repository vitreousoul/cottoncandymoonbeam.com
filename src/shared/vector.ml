module Polar = struct
  type t =
    { r: float
    ; t: float
    }
end

module Cartesian = struct
  type t =
    { x: float
    ; y: float
    }

  let add { x = x1 ; y = y1 } { x = x2 ; y = y2 } =
    { x = x1 +. x2
    ; y = y1 +. y2
    }

  let subtract { x = x1 ; y = y1 } { x = x2 ; y = y2 } =
    { x = x1 -. x2
    ; y = y1 -. y2
    }

  let max { x = x1 ; y = y1 } { x = x2 ; y = y2 } =
    { x = max x1 x2
    ; y = max y1 y2
    }

  let min { x = x1 ; y = y1 } { x = x2 ; y = y2 } =
    { x = min x1 x2
    ; y = min y1 y2
    }

  let of_polar Polar.{ r ; t } =
    { x = r *. cos t
    ; y = r *. sin t
    }
end


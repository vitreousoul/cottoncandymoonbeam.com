import React from 'react'
import { withBaseIcon } from 'react-icons-kit'

import { chevronDown } from 'react-icons-kit/ionicons/chevronDown' 
import { chevronUp } from 'react-icons-kit/ionicons/chevronUp' 
import { minus } from 'react-icons-kit/iconic/minus' 
import { plus } from 'react-icons-kit/iconic/plus' 
import { reload } from 'react-icons-kit/iconic/reload'
import { thickRight } from 'react-icons-kit/iconic/thickRight' 

const ArrowContainer =
    withBaseIcon({ size: 12 })

const ChevronContainer =
    withBaseIcon({ size: 24 })

const PlusMinusContainer =
    withBaseIcon({ size: 24 })

const ReloadContainer =
    withBaseIcon({ size: 16 })

export const ArrowRight = () =>
  <ArrowContainer icon={ thickRight } />

export const ChevronUp = () =>
  <ChevronContainer icon={ chevronUp } />

export const ChevronDown = () =>
  <ChevronContainer icon={ chevronDown } />

export const Plus = () =>
  <PlusMinusContainer icon={ plus } />

export const Minus = () =>
  <PlusMinusContainer icon={ minus } />

export const Reload = () =>
  <ReloadContainer icon={ reload } />


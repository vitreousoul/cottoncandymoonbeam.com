type t =
  { axiom: Symbol.production
  ; rules: Rules.rules
  ; theta: float
  ; depth: int
  }

type action =
  | Rules of Rules.rules
  | Theta of float
  | Depth of int

let pi_over_twelve =
    Js.Math._PI /. 12.

let update_state state = function
  | Rules(rules) ->
      { state with rules = rules }
  | Theta(theta) ->
      { state with theta = theta }
  | Depth(depth) ->
      { state with depth = depth }

let empty =
  { axiom = Symbol.[ A ]
  ; rules = Rules.of_pairs
              Symbol.
              [ ( A, [ A ] )
              ; ( B, [ B ] )
              ]
  ; theta = pi_over_twelve *. 3.
  ; depth = 3
  }


include Map.Make( Symbol )

type rules =
  Symbol.production t

let to_key rules =
  let mapper ( symbol, production ) =
    Symbol.show symbol ^ Symbol.string_of_production production in
  bindings rules
    |> List.rev_map mapper
    |> List.rev
    |> String.concat "-"

let of_pairs pairs =
  let folder rules ( symbol, production ) =
    add symbol production rules in
  pairs
    |> List.fold_left folder empty

let apply symbol rules =
  match find_opt symbol rules with
  | Some( production ) -> production
  | None -> [ symbol ]


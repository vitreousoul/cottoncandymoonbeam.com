type 'a t = 'a Map.Make( Symbol ).t

type key = Symbol.t

type rules = Symbol.production t

val bindings : 'a t -> (key * 'a) list

val add : key -> 'a -> 'a t -> 'a t

val empty : 'a t

val to_key : Symbol.production t -> string

val of_pairs : (key * 'a) list -> 'a t

val apply : key -> key list t -> key list


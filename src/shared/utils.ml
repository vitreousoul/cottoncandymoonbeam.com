let list_rev_mapi f l =
  let rec rmap_f i accu = function
    | [] -> accu
    | a::l -> rmap_f (i + 1) (f i a :: accu) l
  in
  rmap_f 0 [] l
    |> List.rev

let list_flatten lists =
  let folder accu l =
    List.rev_append l accu
  in
  List.fold_left folder [] lists
    |> List.rev


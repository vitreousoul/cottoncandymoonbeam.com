type t =
  | A
  | B
  | Plus
  | Minus
  | Push
  | Pop

[@@deriving ord, show]

type production =
  t list

let to_string = function
  | A -> "a"
  | B -> "b"
  | Plus -> "+"
  | Minus -> "-"
  | Push -> "["
  | Pop -> "]"

let string_of_production production =
  let production_string =
    production
      |> List.rev_map to_string
      |> List.rev in
  String.concat "" production_string


type t =
  { name: string
  ; state: State.t
  }

module Gallery = Map.Make( String )

let make_specimen ~depth ~theta ~name ~pairs =
  { name = name
  ; state =
      { State.empty with
        rules = Rules.of_pairs pairs
      ; depth
      ; theta
      }
  }

let gallery =
  Gallery.empty
    |> Gallery.add
        "fern"
        ( make_specimen
            ~depth: 6
            ~name: "Fern"
            ~theta: State.pi_over_twelve
            ~pairs:
              Symbol.
              [ ( A, [ Minus; B; Plus; Plus; Push; B; Pop ] )
              ; ( B, [ B; Minus; A; Push; A; Minus; A; Pop ] )
              ] )
    |> Gallery.add
        "cascading-mounds"
        ( make_specimen
            ~depth: 5
            ~name: "Cascading Mounds"
            ~theta: ( State.pi_over_twelve *. 2. )
            ~pairs:
              Symbol.
              [ ( A, [ A; A; Push; B; Plus; A; Pop ] )
              ; ( B, [ Minus; B; Plus; B ] )
              ] )
    |> Gallery.add
        "hex-tiles"
        ( make_specimen
            ~depth: 6
            ~name: "Hex Tiles"
            ~theta: ( State.pi_over_twelve *. 2. )
            ~pairs:
              Symbol.
              [ ( A, [ A; A; A; Push; A; Pop; Minus; B; Minus ] )
              ; ( B, [ Minus ] )
              ] )
    |> Gallery.add
        "fuzzy-octagons"
        ( make_specimen
            ~depth: 5
            ~name: "Fuzzy Octagons"
            ~theta: ( State.pi_over_twelve *. 9. )
            ~pairs:
              Symbol.
              [ ( A, [ A; A; A; A; Plus; Push; B; Pop ] )
              ; ( B, [ B; B; B; Push; A; Minus; A; Pop ] )
              ] )

let sierpinski =
  make_specimen
    ~depth: 4
    ~name: "About"
    ~theta: ( State.pi_over_twelve *. 4. )
    ~pairs:
      Symbol.
      [ ( A, [ B; Plus; A; Plus; B ] )
      ; ( B, [ A; Minus; B; Minus; A ] )
      ]

let mosaic =
  make_specimen
    ~depth: 6
    ~name: "Mosaic"
    ~theta: ( State.pi_over_twelve *. 4. )
    ~pairs:
      Symbol.
      [ ( A, [ A; A; A; A; B; Plus ] )
      ; ( B, [ A; B ] )
      ]

let get_gallery key =
  Gallery.find key gallery


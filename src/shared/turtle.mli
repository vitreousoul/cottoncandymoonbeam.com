type frame =
  { heading: Vector.Polar.t
  ; position: Vector.Cartesian.t
  }

type viewbox =
  { min: Vector.Cartesian.t
  ; max: Vector.Cartesian.t
  }

type t =
  { stack: frame Stack.t
  ; frame: frame
  ; path: string
  ; viewbox: viewbox
  }

val of_production : ?theta:float -> Symbol.t list -> t


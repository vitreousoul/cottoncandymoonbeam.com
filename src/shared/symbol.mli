type t =
  | A
  | B
  | Plus
  | Minus
  | Push
  | Pop

type production =
  t list

val to_string : t -> string

val string_of_production : production -> string

val compare : t -> t -> int

val show : t -> string


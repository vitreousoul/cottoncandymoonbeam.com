import React from "react"

import Layout from "../../components/layout"
import SEO from "../../components/seo"
import * as Explore from '../../components/explore.bs.js'
import { get_gallery } from '../../shared/specimen.bs.js'

const IndexPage = () => {
  return (
    <Layout>
      <SEO title={ 'explore' } />
      <Explore.make
        initial_state={ get_gallery( 'hex-tiles' ).state }
      />
    </Layout>
  )
}

export default IndexPage


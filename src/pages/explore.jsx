import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import * as Explore from '../components/explore.bs.js'

const IndexPage = () => {
  return (
    <Layout>
      <SEO title={ 'explore' } />
      <Explore.make />
    </Layout>
  )
}

export default IndexPage


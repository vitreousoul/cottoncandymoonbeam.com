import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import * as Gallery from '../components/gallery_container.bs.js'

const IndexPage = () => {
  return (
    <Layout>
      <SEO title={ 'gallery' } />
      <Gallery.make />
    </Layout>
  )
}

export default IndexPage


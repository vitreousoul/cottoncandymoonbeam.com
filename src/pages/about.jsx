import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'
import * as SpecimenCard from '../components/specimen_card.bs.js'
import * as Grid from '../components/grid.bs.js'
import { sierpinski } from '../shared/specimen.bs.js'

export const query = graphql`
  query {
    allMarkdownRemark {
      nodes {
        htmlAst
      }
    }
  }`

const IndexPage = ({ data }) => {
  const htmlChildren =
    data.allMarkdownRemark.nodes.length > 0
      ? data.allMarkdownRemark.nodes[0].htmlAst.children
      : []

  const isAboutSection = child =>
    child.properties && child.properties.id === 'about'

  const joinChildText = child =>
    child.children
      .map( c => c.value )
      .join(' ')

  const extractParagraphs = ( child ) =>
    child.children
      .filter( c => c.tagName === 'p' )
      .map( joinChildText )
      .map( ( text, i ) => <p key={ i }>{ text }</p> )

  const { axiom, rules, theta, depth } = sierpinski.state

  return (
    <Layout>
      <SEO title={ 'about' } />
      <Grid.make>
        <div className="col-12 col-md-7">
          <h1>{ "About" }</h1>
          { htmlChildren
              .filter( isAboutSection )
              .map( extractParagraphs ) }
        </div>
        <figure className="col-12 col-md-5">
          <SpecimenCard.make
            production={ axiom }
            rules={ rules }
            theta={ theta }
            depth={ depth }
          />
        </figure>
      </Grid.make>
    </Layout>
  )
}

export default IndexPage


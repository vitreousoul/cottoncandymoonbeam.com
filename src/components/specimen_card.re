[@react.component]
let make = (
  ~production,
  ~rules,
  ~theta,
  ~depth = 3,
) => {
  let production = Lsystem.produce( production, rules, depth );

  let turtle = Turtle.of_production( production, ~theta );

  let viewbox = {
    let padding = 10.;
    let size = Vector.Cartesian.subtract( turtle.viewbox.max, turtle.viewbox.min );
    [ turtle.viewbox.min.x -. padding
    , turtle.viewbox.min.y -. padding
    , size.x +. 2. *. padding
    , size.y +. 2. *. padding
    ]
      |> List.map @@ Js.Float.toString
      |> String.concat @@ " "
  };

  <figure className="specimen">
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox={viewbox}
    >
      <path d=turtle.path />
    </svg>
  </figure>
};


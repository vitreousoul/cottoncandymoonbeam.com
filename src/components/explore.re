[@react.component]
let make = (
  ~initial_state = State.empty,
) => {
  let ( state, set_state ) =
    React.useState( _ => initial_state );

  let set_rules = rules =>
    set_state @@ _ =>
      State.update_state( state, State.Rules(rules) );

  let set_depth = depth =>
    set_state @@ _ =>
      State.update_state( state, State.Depth(depth) );

  let set_theta = theta =>
    set_state @@ _ => {
      let theta =
        Js.Float.fromString @@ theta;
      State.update_state( state, State.Theta(theta) )
    };

  <section className="explore">
    <div className="heading">
      <Rules_container.make rules={ state.rules } />
      <button onClick={ _ => set_state( _ => State.empty ) }>
        <Reload />
      </button>
    </div>
    <div className="specimen-container">
      <article className="aspect-square-container">
        <div className="aspect-square">
          <Specimen_card
            rules={ state.rules }
            production={ Lsystem.axiom }
            depth={ state.depth }
            theta={ state.theta }
          />
        </div>
      </article>
    </div>
    <Settings
      depth={ state.depth }
      theta={ state.theta }
      set_depth
      set_theta
    />
    <Neighbors
      rules={ state.rules }
      set_rules
      theta={ state.theta }
    />
  </section>
};


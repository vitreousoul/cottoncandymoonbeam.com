type range =
  { id: string
  , min: float
  , max: float
  , step: float
  , default_value: string
  , on_change: ReactEvent.Form.t => unit
  }

@react.component
let make = (
  ~depth,
  ~theta,
  ~set_depth,
  ~set_theta,
) => {
  let pi_over_twelve =
    Js.Math._PI /. 12. 

  let string_of_float = n =>
    Js.Float.toString( n )

  let change_handler = (handler, e) =>
    handler( ReactEvent.Form.target( e )[ "value" ] )

  let settings =
    list{
      { id: "theta"
      , min: pi_over_twelve
      , max: pi_over_twelve *. 11.
      , step: pi_over_twelve
      , default_value: string_of_float( theta )
      , on_change: change_handler( set_theta )
      }
    , { id: "depth"
      , min: 1.
      , max: 6.
      , step: 1.
      , default_value: string_of_int( depth )
      , on_change: change_handler( set_depth )
      }
    }

  <section className="settings">
    <Grid>
      { Element.map( settings, ({ id, min, max, step, default_value, on_change }) =>
          <div
            key={ id }
            className="setting col-12 col-md-6"
          >
            <div className="row no-gutters align-items-center">
              <label
                htmlFor={ id }
                className="col-2"
              >
                { React.string( id ) }
              </label>
              <div className="col input-container">
                <input
                  id={ id }
                  type_="range"
                  min={ string_of_float( min ) }
                  max={ string_of_float( max ) }
                  step={ step }
                  defaultValue={ default_value }
                  onChange={ on_change }
                />
              </div>
            </div>
          </div> )}
    </Grid>
  </section>
}


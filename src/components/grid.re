[@react.component]
let make = (
  ~fluid = false,
  ~children,
  ~rowClass = ""
) => {
  let container =
    switch ( fluid ) {
    | true => "fluid-container"
    | false => "container"
    };

  <div className={ container }>
    <div className={ "row" ++ " " ++ rowClass }>
      { children }
    </div>
  </div>  
};


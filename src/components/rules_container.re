[@react.component]
let make = (~rules) => {
  <article className="rules">
    { Element.map( Rules.bindings( rules ), ( (s, p) ) => {
        <div key={ Symbol.show @@ s }>
          <Rule symbol={ s } production={ p } />
        </div> })}
  </article>
};


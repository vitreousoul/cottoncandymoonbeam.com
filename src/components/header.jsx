import React from 'react'
import PropTypes from 'prop-types'

import Nav from './nav'

const Header = ({ siteTitle }) => (
  <header>
    <section>
      <h1>{ siteTitle }</h1>
      <Nav />
    </section>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header


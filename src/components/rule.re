[@react.component]
let make = (~symbol, ~production) => {
  let symbol_string = symbol =>
    React.string @@ Symbol.to_string( symbol );

  <section className="rule">
    <span>{ symbol_string @@ symbol }</span>
    <Arrow_right />
    <span>
      { production
          |> Symbol.string_of_production
          |> React.string }
    </span>
  </section>
};


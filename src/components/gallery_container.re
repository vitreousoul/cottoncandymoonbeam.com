module GalleryCard = {
  [@bs.module "./GalleryCard.jsx"][@react.component]
  external make: (~specimen: Specimen.t, ~slug: string) => React.element = "default";
}

[@react.component]
let make = () => {
  <section className="gallery">
    <Grid>
      { Specimen.( Gallery.bindings @@ gallery ) -> Element.map( _, ( (slug, specimen) ) =>
          <GalleryCard
            key={ slug }
            slug={ slug }
            specimen={ specimen }
          /> )}
    </Grid>
  </section>
};


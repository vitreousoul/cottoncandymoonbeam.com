[@bs.val][@bs.scope "window"] external scroll : (int, int) => unit = "scroll";

[@react.component]
let make = (
  ~rules,
  ~set_rules,
  ~theta,
) => {
  let scrollToTop = () =>
    scroll( 0, 0 );

  let set_rules = rules => {
    scrollToTop();
    set_rules @@ rules;
  };

  let replacements =
    Replacement.replacements_of_rules @@ rules;

  let extensions =
    Replacement.extensions_of_rules @@ rules;

  let stacks =
    Stacker.stacks_of_rules @@ rules;

  <section className="neighbors">
    <Neighbor_card
      rules_list={ replacements }
      set_rules
      theta
      key_prefix="replacements"
    />
    <Neighbor_card
      rules_list={ extensions }
      set_rules
      theta
      key_prefix="extensions"
    />
    <Neighbor_card
      rules_list={ stacks }
      set_rules
      theta
      key_prefix="stacks"
    />
  </section>
};


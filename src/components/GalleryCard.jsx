import React from 'react'
import { Link } from "@reach/router"

import * as Rules from './rules_container.bs.js'
import * as SpecimenCard from './specimen_card.bs.js'

const GalleryCard = ({ specimen, slug }) => {
  const {
    name,
    state: { depth, theta, axiom, rules }
  } = specimen

  return (
  <article className="gallery-card col-12 col-md-6">
    <Link to={ `/explore/${ slug }` }>
      <h2>{ name }</h2>
    </Link>
    <Rules.make rules={ rules } />
    <section>
      <SpecimenCard.make
        production={ axiom }
        rules={ rules }
        theta={ theta }
        depth={ depth }
      />
    </section>
  </article> )
};

export default GalleryCard


[@bs.val][@bs.scope "window"] external scroll : (int, int) => unit = "scroll";

[@react.component]
let make = (
  ~index,
  ~rules,
  ~symbol,
  ~production,
  ~set_rules,
  ~theta,
) => {
  let add_symbol = replacement =>
    Rules.add( symbol, replacement, rules );

  let replacement_rules_list =
    Replacement.replacements_of_production( index, production )
      |> List.map @@ add_symbol;

  let onClick = ( replacement_rules ) => {
    scroll( 0, 0 );
    set_rules @@ replacement_rules;
  };

  <section className="replacements">
    { Element.map( replacement_rules_list, replacement_rules =>
        <article key={ "replacements" ++ Rules.to_key @@ replacement_rules }>
          <Rules_container rules={ replacement_rules } />
          <div className="aspect-square-container">
            <button
              className="aspect-square"
              onClick={ _ => onClick @@ replacement_rules }
            >
              <Specimen_card
                production={ Lsystem.axiom }
                rules={ replacement_rules }
                theta
              />
            </button>
          </div>
        </article> )}
  </section>
};


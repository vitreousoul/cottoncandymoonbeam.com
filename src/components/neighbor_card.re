[@react.component]
let make = (
  ~rules_list,
  ~set_rules,
  ~theta,
  ~key_prefix=""
) => {
  let onClick = replacement_rules =>
    set_rules @@ replacement_rules;

  let title = rules => {
    let mapper = (( symbol, production )) =>
        Symbol.to_string( symbol ) ++ " -> " ++ Symbol.string_of_production( production );

    Rules.bindings( rules )
      |> List.map( mapper )
      |> String.concat( "\n" )
  };

  <section className="neighbor-card">
    <Grid>
      { rules_list |> Element.mapi @@ ( i, rules ) =>
          <div
            key={ key_prefix ++ string_of_int @@ i }
            className="neighbor col-6 col-sm-4 col-md-3 col-lg-2"
          >
            <button
              className="aspect-square-container"
              title={title @@ rules}
            >
              <article
                className="aspect-square"
                onClick={ _ => onClick @@ rules }
              >
                <Specimen_card
                  production={ Lsystem.axiom }
                  rules
                  theta
                />
              </article>
            </button>
          </div> }
    </Grid>
  </section>
};


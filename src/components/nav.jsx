import React from 'react'
import { Link } from 'gatsby'

const Nav = () => (
  <nav>
    <span>
      <Link to='/gallery'>Gallery</Link>
    </span>
    <span>
      <Link to='/explore'>Explore</Link>
    </span>
    <span>
      <Link to='/about'>About</Link>
    </span>
  </nav>
)

export default Nav


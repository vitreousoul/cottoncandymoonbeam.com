type line_style =
  | Solid
  | Dashed
  ;

let string_of_line_style = line_style =>
  switch (line_style) {
  | Solid => "solid"
  | Dashed => "dashed"
  };

[@react.component]
let make = (~lineStyle, ~children=React.null) => {
  <section className="divider">
    <div className={ string_of_line_style @@ lineStyle }></div>
    { children }
    <div className={ string_of_line_style @@ lineStyle }></div>
  </section>
};

